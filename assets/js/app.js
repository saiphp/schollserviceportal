new Vue({
	el: 'body',
	data: {
		message: 'hello',
		show_add: false,
		show_first_page: true,
		base: 'http://localhost/portal/',
	},

	methods: {
		showAdd: function () {
			this.show_add = true
			this.show_first_page = false
		},

		addSchool: function () {

			var form = $('form')[0];
			var fd = new FormData(form);
					console.log(fd);

			$.ajax({
				type: "POST",
				data: fd,
				processData: false,
  				contentType: false,
				url: this.base + 'admin/addSchool',
				success: function() {
					alert('added successfully');
				}
			})
		}
	}
});