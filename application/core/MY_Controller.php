<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    function __construct()
    {
        parent::__construct(); 
        
    }

    public function main_view($view)
    {
        $this->load->view('template/header');
        $this->load->view('template/navbar');
        $this->load->view($view);
        $this->load->view('template/footer');
    }

    public function template_view($view)
    {
        $this->load->view('template/header');
        $this->load->view($view);
        $this->load->view('template/footer');
    }

}
