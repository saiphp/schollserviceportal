<style>
    .login-form {
        margin-top: 100px;
    }
</style>
<div class="container">
    <div class="col-md-4 pull-right login-form">
        <div class="panel panel-default">
            <div class="panel-body">
                <h3>Login</h3>
                <form action="<?php base_url();?>login" method="POST">
                    <div class="form-group">
                        <input type="text" class="form-control" name="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" class="form-control" name="password" placeholder="Password">
                    </div>
                    <input type="submit" class="btn btn-primary pull-right" value="Login">
                </form>
                <?php echo validation_errors(); ?>
                <?php echo $this->session->flashdata('error'); ?>

            </div>
        </div>
    </div>
</div>
