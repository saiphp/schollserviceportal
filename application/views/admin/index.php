<div class="" v-if="show_first_page">
	<div class="col-md-3">
		<div class="panel panel-default wow fadeIn">
		  <div class="panel-body">
		    <h5>Schools</h5>
		    <p>
		    	Add School first.
		    </p>
		    <br>
		    <br>
		    <button @click="showAdd()" class="btn btn-primary pull-right pull-bottom">Add</button>
		  </div>
		</div>
	</div>

	<div class="col-md-3">
		<div class="panel panel-default wow fadeIn">
		  <div class="panel-body">
		    <h5>School Admins</h5>
		    <p>
		    	Then add school admin.
		    </p>
		    <br>
		    <br>
		    <button @click="showAdd()" class="btn btn-primary pull-right pull-bottom">Add</button>
		  </div>
		</div>
	</div>

</div>


<div class="col-md-3" v-if="show_add" v-cloak>

	<div class="panel panel-default wow fadeIn">
		<div class="panel-body">
			<form action="<?php echo base_url(); ?>admin/addSchool" method="POST">
				<div class="form-group">
					<input type="text" name="schoolname" class="form-control" placeholder="School Name">
				</div>
				<div class="form-group">
					<input type="text" name="description" class="form-control" placeholder="Description">
				</div>
				<div class="form-group">
					<input @click="addSchool" class="btn btn-primary pull-right" value="Add">
				</div>
			</form>
			<a href=""><button class="btn btn-danger">Back</button></a>
		</div>
	</div>
</div>



