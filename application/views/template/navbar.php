<nav class="navbar navbar-default navbar-static-top">
  <div class="container">
    <ul class="nav navbar-nav">
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="">Hi! <?= $this->session->userdata('name'); ?></a></li>
      <li class="pull-right"><a href="<?php echo base_url(); ?>login/logout">logout</a></li>
    </ul>
  </div>
</nav>